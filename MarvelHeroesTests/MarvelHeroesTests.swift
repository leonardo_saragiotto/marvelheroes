//
//  MarvelHeroesTests.swift
//  MarvelHeroesTests
//
//  Created by Leonardo Saragiotto on 23/04/19.
//  Copyright © 2019 Leonardo Saragiotto. All rights reserved.
//

import XCTest
import MarvelHeroes

class MarvelHeroesTests: XCTestCase {
    
    var request: MarvelRequestProtocol?
    var timeout = 5.0

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testHeroList() {
        let expectation = self.expectation(description: "")
        request = MarvelRequest()
        
        request?.getHeroes({ (error, response) in
            XCTAssertNil(error)
            XCTAssertNotNil(response)
            XCTAssertEqual(response?.offset, 0)
            XCTAssertEqual(response?.limit, 20)
            XCTAssertEqual(response?.total, 1491)
            XCTAssertEqual(response?.count, 20)
            
            expectation.fulfill()
        })
        
        waitForExpectations(timeout: timeout, handler: nil)
    }

    func testHeroCommics() {
        let expectation = self.expectation(description: "")
        let heroId = "1011334"
        request = MarvelRequest()
        
        request?.getContent(heroId, type: .comics, completion: { (error, response) in
            XCTAssertNil(error)
            XCTAssertNotNil(response)
            XCTAssertEqual(response?.offset, 0)
            XCTAssertEqual(response?.limit, 20)
            XCTAssertEqual(response?.total, 12)
            XCTAssertEqual(response?.count, 12)
            
            expectation.fulfill()
        })
        
        waitForExpectations(timeout: timeout, handler: nil)
    }
    
    func testHeroEvents() {
        let expectation = self.expectation(description: "")
        let heroId = "1011334"
        request = MarvelRequest()
        
        request?.getContent(heroId, type: .events, completion: { (error, response) in
            XCTAssertNil(error)
            XCTAssertNotNil(response)
            XCTAssertEqual(response?.offset, 0)
            XCTAssertEqual(response?.limit, 20)
            XCTAssertEqual(response?.total, 1)
            XCTAssertEqual(response?.count, 1)
            
            expectation.fulfill()
        })
        
        waitForExpectations(timeout: timeout, handler: nil)
    }
    
    func testHeroSeries() {
        let expectation = self.expectation(description: "")
        let heroId = "1011334"
        request = MarvelRequest()
        
        request?.getContent(heroId, type: .series, completion: { (error, response) in
            XCTAssertNil(error)
            XCTAssertNotNil(response)
            XCTAssertEqual(response?.offset, 0)
            XCTAssertEqual(response?.limit, 20)
            XCTAssertEqual(response?.total, 3)
            XCTAssertEqual(response?.count, 3)
            
            expectation.fulfill()
        })
        
        waitForExpectations(timeout: timeout, handler: nil)
    }
    
    func testHeroStories() {
        let expectation = self.expectation(description: "")
        let heroId = "1011334"
        request = MarvelRequest()
        
        request?.getContent(heroId, type: .stories, completion: { (error, response) in
            XCTAssertNil(error)
            XCTAssertNotNil(response)
            XCTAssertEqual(response?.offset, 0)
            XCTAssertEqual(response?.limit, 20)
            XCTAssertEqual(response?.total, 21)
            XCTAssertEqual(response?.count, 20)
            
            expectation.fulfill()
        })
        
        waitForExpectations(timeout: timeout, handler: nil)
    }
    
    func testImagePath() {
        let expectation = self.expectation(description: "")
        let heroId = "1011334"
        request = MarvelRequest()
        
        request?.getContent(heroId, type: .comics, completion: { (error, response) in
            XCTAssertNil(error)
            XCTAssertNotNil(response)
            
            XCTAssertNotNil(response?.content?.first?.thumbnail)
            
            if let thumb = response?.content?.first?.thumbnail {
                XCTAssertEqual(thumb.imagePath(for: .small), "http://i.annihil.us/u/prod/marvel/i/mg/d/03/58dd080719806/portrait_small.jpg")
                XCTAssertEqual(thumb.imagePath(for: .medium), "http://i.annihil.us/u/prod/marvel/i/mg/d/03/58dd080719806/portrait_medium.jpg")
                XCTAssertEqual(thumb.imagePath(for: .xlarge), "http://i.annihil.us/u/prod/marvel/i/mg/d/03/58dd080719806/portrait_xlarge.jpg")
                XCTAssertEqual(thumb.imagePath(for: .fantastic), "http://i.annihil.us/u/prod/marvel/i/mg/d/03/58dd080719806/portrait_fantastic.jpg")
                XCTAssertEqual(thumb.imagePath(for: .uncanny), "http://i.annihil.us/u/prod/marvel/i/mg/d/03/58dd080719806/portrait_uncanny.jpg")
                XCTAssertEqual(thumb.imagePath(for: .incredible), "http://i.annihil.us/u/prod/marvel/i/mg/d/03/58dd080719806/portrait_incredible.jpg")
                XCTAssertEqual(thumb.imagePath(for: .full), "http://i.annihil.us/u/prod/marvel/i/mg/d/03/58dd080719806.jpg")
            }
            
            expectation.fulfill()
        })
        
        waitForExpectations(timeout: timeout, handler: nil)
    }
}
