# Marvel Heroes

## Instalation guide

 - Create a property list file named MarvelKey.plist
 - Include two string values as shown bellow
 - Fill this property with the public and private key provided by [Marvel Developer API](https://developer.marvel.com/)

 ![N|Solid](https://bitbucket.org/leonardo_saragiotto/marvelheroes/raw/a3668a9c2c57c7037a2bc3d045b3163f638e884e/Screen%20Shot%202019-04-26%20at%2014.02.37.png)

