//
//  HeroListViewModel.swift
//  MarvelHeroes
//
//  Created by Leonardo Saragiotto on 25/04/19.
//  Copyright © 2019 Leonardo Saragiotto. All rights reserved.
//

import Foundation

class HeroListViewModel {
    let marvelRequest: MarvelRequestProtocol
    
    var heroesLoaded: (() -> Void)?
    var newHeroesLoaded: (([IndexPath]) -> Void)?
    var whenSometingIsNotRight: ((String) -> Void)?
    
    var numberOfHeroes: Int {
        return heroesList?.heroes?.count ?? 0
    }
    
    private var heroesList: MarvelHeroesList?
    
    init() {
        marvelRequest = MarvelRequest()
    }
    
    func loadList() {
        marvelRequest.getHeroes(0) { [weak self] (error, heroesResponse) in
            if let error = error {
                self?.whenSometingIsNotRight?(error.errorDescription)
                return
            }
            
            if let heroesResponse = heroesResponse {
                self?.heroesList = heroesResponse
                self?.heroesLoaded?()
            }
        }
    }
    
    func loadMoreHeroes() {
        var offset = 0.0
        
        if let heroesList = heroesList, let count = heroesList.count, let currentOffset = heroesList.offset {
            offset = count + currentOffset
            
            if let totalHeroes = heroesList.total, offset >= totalHeroes {
                return
            }
        }
        
        marvelRequest.getHeroes(offset) { [weak self] (error, heroesResponse) in
            if let error = error {
                self?.whenSometingIsNotRight?(error.errorDescription)
                return
            }
            
            if let heroesResponse = heroesResponse, let heroesList = heroesResponse.heroes {
                self?.heroesList?.count = heroesResponse.count
                self?.heroesList?.offset = heroesResponse.offset
                self?.heroesList?.heroes? += heroesList
                
                var newIndexes = [IndexPath]()
                let startIndex = Int(self?.heroesList?.heroes?.count ?? 0) - Int(heroesResponse.offset ?? 0)
                
                for row in 0..<Int(self?.heroesList?.count ?? 0) {
                    newIndexes.append(IndexPath(row: startIndex + row, section: 0))
                }
                
                self?.newHeroesLoaded?(newIndexes)
            }
        }
    }
    
    func hero(for indexPath: IndexPath) -> MarvelHero? {
        return heroesList?.heroes?[indexPath.row]
    }
}
