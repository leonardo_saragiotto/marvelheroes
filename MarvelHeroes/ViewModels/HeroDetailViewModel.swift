//
//  HeroDetailViewModel.swift
//  MarvelHeroes
//
//  Created by Leonardo Saragiotto on 27/04/19.
//  Copyright © 2019 Leonardo Saragiotto. All rights reserved.
//

import Foundation

class HeroDetailViewModel {
    let marvelRequest: MarvelRequestProtocol
    
    var detailLoaded: ((HeroContentType?) -> Void)?
    var whenSometingIsNotRight: ((String) -> Void)?
    
    private var heroDetail: MarvelHeroContentList?
    private var comicsDetail: MarvelHeroContentList?
    private var eventsDetail: MarvelHeroContentList?
    private var seriesDetail: MarvelHeroContentList?
    private var storiesDetail: MarvelHeroContentList?
    
    init() {
        marvelRequest = MarvelRequest()
    }
    
    func loadHeroDetail(_ heroId: Double) {
        marvelRequest.getHeroDetail(heroId) { [weak self] (error, response) in
            if let error = error {
                self?.whenSometingIsNotRight?(error.errorDescription)
                return
            }
            
            if let response = response {
                self?.heroDetail = response
                self?.detailLoaded?(response.contentType)
            }
        }
        
        marvelRequest.getContent(heroId, type: .comics) { [weak self] (error, response) in
            if let error = error {
                self?.whenSometingIsNotRight?(error.errorDescription)
                return
            }
            
            if let response = response {
                self?.comicsDetail = response
                self?.detailLoaded?(response.contentType)
            }
        }
        
        marvelRequest.getContent(heroId, type: .events) { [weak self] (error, response) in
            if let error = error {
                self?.whenSometingIsNotRight?(error.errorDescription)
                return
            }
            
            if let response = response {
                self?.eventsDetail = response
                self?.detailLoaded?(response.contentType)
            }
        }
        
        marvelRequest.getContent(heroId, type: .series) { [weak self] (error, response) in
            if let error = error {
                self?.whenSometingIsNotRight?(error.errorDescription)
                return
            }
            
            if let response = response {
                self?.seriesDetail = response
                self?.detailLoaded?(response.contentType)
            }
        }
        
        marvelRequest.getContent(heroId, type: .stories) { [weak self] (error, response) in
            if let error = error {
                self?.whenSometingIsNotRight?(error.errorDescription)
                return
            }
            
            if let response = response {
                self?.storiesDetail = response
                self?.detailLoaded?(response.contentType)
            }
        }
    }
    
    func sectoinTitle(for section: Int) -> String {
        switch section {
        case 0:
            return "Hero"
        case 1:
            return "Comics"
        case 2:
            return "Events"
        case 3:
            return "Series"
        case 4:
            return "Stories"
        default:
            return "To be Defined"
        }
    }
    
    func numberOfRows(for section: Int) -> Int {
        var rows = 0
        switch section {
        case 0:
            rows = 1
        case 1:
            rows = comicsDetail?.content?.count ?? 0
        case 2:
            rows = eventsDetail?.content?.count ?? 0
        case 3:
            rows = seriesDetail?.content?.count ?? 0
        case 4:
            rows = storiesDetail?.content?.count ?? 0
        default:
            return 0
        }
        
        return (rows > 3) ? 3 : rows
    }
    
    func heroContent(for indexPath: IndexPath) -> MarvelHeroContent? {
        switch indexPath.section {
        case 0:
            return heroDetail?.content?.first
        case 1:
            return comicsDetail?.content?[indexPath.row]
        case 2:
            return eventsDetail?.content?[indexPath.row]
        case 3:
            return seriesDetail?.content?[indexPath.row]
        case 4:
            return storiesDetail?.content?[indexPath.row]
        default:
            return nil
        }
    }
}
