//
//  HeroesDetailViewController.swift
//  MarvelHeroes
//
//  Created by Leonardo Saragiotto on 25/04/19.
//  Copyright © 2019 Leonardo Saragiotto. All rights reserved.
//

import UIKit

class HeroesDetailViewController: UIViewController {
    
    var customView: HeroesDetailView?
    var viewModel: HeroDetailViewModel?
    var hero: MarvelHero?
    
    enum Titles: String {
        case Loading
    }
    
    override func loadView() {
        customView = HeroesDetailView()
        customView?.delegate = self
        view = customView
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        customView?.setupView()
        viewModel = HeroDetailViewModel()
        viewModel?.detailLoaded = { [weak self] type in
            self?.title = self?.hero?.name
            self?.customView?.heroesTableView?.reloadData()
        }
        viewModel?.loadHeroDetail(hero?.id ?? 0)
        title = Titles.Loading.rawValue
    }
    
    func setDelegate(for tableView: UITableView) {
        tableView.delegate = self
        tableView.dataSource = self
    }
}

extension HeroesDetailViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        if let view = view as? UITableViewHeaderFooterView {
            view.textLabel?.textColor = .white
            view.textLabel?.font = UIFont.boldSystemFont(ofSize: 14.0)
        }
    }
}

extension HeroesDetailViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.numberOfRows(for: section) ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "detailCell", for: indexPath) as? HeroesDetailViewCell else {
            return UITableViewCell()
        }
        
        cell.configure(with: viewModel?.heroContent(for: indexPath))
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return viewModel?.sectoinTitle(for: section)
    }
    
}
