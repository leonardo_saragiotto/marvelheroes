//
//  HeroesListViewController.swift
//  MarvelHeroes
//
//  Created by Leonardo Saragiotto on 24/04/19.
//  Copyright © 2019 Leonardo Saragiotto. All rights reserved.
//

import UIKit

class HeroesListViewController: UIViewController {
    
    var customView: HeroesListView?
    var viewModel: HeroListViewModel?
    var isLoadingHeroes = false
    
    enum Titles: String {
        case Heroes, Loading
    }
    
    override func loadView() {
        customView = HeroesListView()
        customView?.delegate = self
        view = customView
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        title = Titles.Loading.rawValue
        customView?.setupView()
        viewModel = HeroListViewModel()
        viewModel?.heroesLoaded = { [weak self] in
            self?.customView?.heroesCollectionView?.reloadData()
            self?.isLoadingHeroes = false
            self?.title = Titles.Heroes.rawValue
        }
        viewModel?.newHeroesLoaded = { [weak self] (indexes) in
            self?.customView?.heroesCollectionView?.insertItems(at: indexes)
            self?.isLoadingHeroes = false
            self?.title = Titles.Heroes.rawValue
        }
        viewModel?.loadList()
        isLoadingHeroes = true
    }
    
    func setDelegate(for collectionView: UICollectionView) {
        collectionView.delegate = self
        collectionView.dataSource = self
    }

}

extension HeroesListViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel?.numberOfHeroes ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "heroCell", for: indexPath) as? HeroesListViewCell {
            cell.configure(with: viewModel?.hero(for: indexPath))
            return cell
        }
        return UICollectionViewCell()
    }
}

extension HeroesListViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if let lastItem = viewModel?.numberOfHeroes, indexPath.row > lastItem - 5 {
            if !isLoadingHeroes {
                isLoadingHeroes = true
                title = Titles.Loading.rawValue
                viewModel?.loadMoreHeroes()
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let detailViewController = HeroesDetailViewController()
        detailViewController.hero = viewModel?.hero(for: indexPath)
        self.navigationController?.pushViewController(detailViewController, animated: true)
    }
}
