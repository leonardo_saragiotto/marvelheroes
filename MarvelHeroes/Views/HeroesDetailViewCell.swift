//
//  HeroesDetailViewCell.swift
//  MarvelHeroes
//
//  Created by Leonardo Saragiotto on 27/04/19.
//  Copyright © 2019 Leonardo Saragiotto. All rights reserved.
//

import UIKit

class HeroesDetailViewCell: UITableViewCell {
    
    var nameLabel: UILabel!
    var thumbImageView: UIImageView!
    var textDescription: UITextView!

    let spacingCell: CGFloat = 8.0
    let nameLabelFontSize: CGFloat = 12.0
    let textDescFontSize: CGFloat = 14.0
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    private func setupView() {
        backgroundColor = .clear
        
        thumbImageView = UIImageView(frame: .zero)
        addSubview(thumbImageView)
        
        thumbImageView.translatesAutoresizingMaskIntoConstraints = false
        thumbImageView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: spacingCell).isActive = true
        thumbImageView.topAnchor.constraint(equalTo: topAnchor, constant: spacingCell).isActive = true
        thumbImageView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -spacingCell).isActive = true
        thumbImageView.widthAnchor.constraint(equalToConstant: 60.0).isActive = true
        thumbImageView.contentMode = .scaleAspectFill
        thumbImageView.clipsToBounds = true
        thumbImageView.backgroundColor = .clear
        
        nameLabel = UILabel(frame: .zero)
        addSubview(nameLabel)
        
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        nameLabel.font = UIFont.systemFont(ofSize: nameLabelFontSize)
        nameLabel.leadingAnchor.constraint(equalTo: thumbImageView.trailingAnchor, constant: spacingCell).isActive = true
        nameLabel.topAnchor.constraint(equalTo: topAnchor, constant: spacingCell).isActive = true
        nameLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -spacingCell).isActive = true
        nameLabel.heightAnchor.constraint(equalToConstant: textDescFontSize)
        nameLabel.textColor = .white
        nameLabel.backgroundColor = .clear
        nameLabel.numberOfLines = 1
        nameLabel.setContentHuggingPriority(.defaultHigh, for: .vertical)
        
        textDescription = UITextView(frame: .zero)
        addSubview(textDescription)
        
        textDescription.translatesAutoresizingMaskIntoConstraints = false
        nameLabel.font = UIFont.systemFont(ofSize: 10.0)
        textDescription.leadingAnchor.constraint(equalTo: thumbImageView.trailingAnchor, constant: spacingCell).isActive = true
        textDescription.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: spacingCell).isActive = true
        textDescription.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -spacingCell).isActive = true
        textDescription.bottomAnchor.constraint(equalTo: thumbImageView.bottomAnchor).isActive = true
        textDescription.textColor = .lightGray
        textDescription.backgroundColor = .clear
        textDescription.textContainerInset = UIEdgeInsets.zero
        textDescription.isScrollEnabled = false
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func configure(with heroContent: MarvelHeroContent?) {
        nameLabel.text = heroContent?.title
        textDescription.text = heroContent?.description ?? " "
        thumbImageView.loadImage(heroContent?.thumbnail?.imagePath(for: .full))
    }
}
