//
//  HeroesDetailView.swift
//  MarvelHeroes
//
//  Created by Leonardo Saragiotto on 25/04/19.
//  Copyright © 2019 Leonardo Saragiotto. All rights reserved.
//

import UIKit

class HeroesDetailView: UIView {

    var delegate: HeroesDetailViewController?
    var heroesTableView: UITableView?
    
    func setupView() {
        heroesTableView = UITableView(frame: .zero, style: .grouped)
        heroesTableView?.backgroundColor = .black
        heroesTableView?.register(HeroesDetailViewCell.self, forCellReuseIdentifier: "detailCell")
        heroesTableView?.rowHeight = 120.0
        heroesTableView?.separatorColor = .clear
        heroesTableView?.allowsSelection = false
        
        if let tableView = heroesTableView {
            addSubview(tableView)
            constraintHeroTableView()
            delegate?.setDelegate(for: tableView)
        }
    }
    
    func constraintHeroTableView() {
        heroesTableView?.translatesAutoresizingMaskIntoConstraints = false
        heroesTableView?.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        heroesTableView?.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        heroesTableView?.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        heroesTableView?.topAnchor.constraint(equalTo: topAnchor).isActive = true
    }
}
