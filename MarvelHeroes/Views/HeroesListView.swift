//
//  HeroesListView.swift
//  MarvelHeroes
//
//  Created by Leonardo Saragiotto on 24/04/19.
//  Copyright © 2019 Leonardo Saragiotto. All rights reserved.
//

import UIKit

class HeroesListView: UIView {

    var delegate: HeroesListViewController?
    var heroesCollectionView: UICollectionView?
    
    func setupView() {
        heroesCollectionView = UICollectionView(frame: frame, collectionViewLayout: heroLayout())
        heroesCollectionView?.backgroundColor = .black
        heroesCollectionView?.register(HeroesListViewCell.self, forCellWithReuseIdentifier: "heroCell")
        
        if let heroView = heroesCollectionView {
            addSubview(heroView)
            constraintHeroCollectionView()
            delegate?.setDelegate(for: heroView)
        }
    }
    
    func constraintHeroCollectionView() {
        heroesCollectionView?.translatesAutoresizingMaskIntoConstraints = false
        heroesCollectionView?.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        heroesCollectionView?.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        heroesCollectionView?.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        heroesCollectionView?.topAnchor.constraint(equalTo: topAnchor).isActive = true
    }
    
    private func heroLayout() -> UICollectionViewLayout {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        let screenWidth = UIScreen.main.bounds.width
        let cellSpacing: CGFloat = 2.0
        let cellWidth = (screenWidth - (cellSpacing * 3)) / cellSpacing
        
        layout.sectionInset = UIEdgeInsets(top: cellSpacing, left: cellSpacing,
                                           bottom: cellSpacing, right: cellSpacing)
        layout.itemSize = CGSize(width: cellWidth, height: 320)
        layout.minimumInteritemSpacing = cellSpacing
        layout.minimumLineSpacing = cellSpacing
        
        return layout
    }
}
