//
//  HeroesListViewCell.swift
//  MarvelHeroes
//
//  Created by Leonardo Saragiotto on 24/04/19.
//  Copyright © 2019 Leonardo Saragiotto. All rights reserved.
//

import UIKit

class HeroesListViewCell: UICollectionViewCell {
    
    var nameLabel: UILabel!
    var heroImageView: UIImageView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupLabel()
        setupImageView()
    }
    
    private func setupLabel() {
        nameLabel = UILabel(frame: .zero)
        addSubview(nameLabel)
        
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        nameLabel.font = UIFont.systemFont(ofSize: 8.0)
        nameLabel.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        nameLabel.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        nameLabel.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        nameLabel.heightAnchor.constraint(equalToConstant: 12.0)
        nameLabel.textAlignment = .center
        nameLabel.textColor = .white
        nameLabel.setContentCompressionResistancePriority(.defaultHigh, for: .vertical)
    }
    
    private func setupImageView() {
        heroImageView = UIImageView(frame: .zero)
        addSubview(heroImageView)
        
        heroImageView.translatesAutoresizingMaskIntoConstraints = false
        heroImageView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        heroImageView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        heroImageView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        heroImageView.bottomAnchor.constraint(equalTo: nameLabel.topAnchor).isActive = true
        heroImageView.contentMode = .scaleAspectFill
        heroImageView.clipsToBounds = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) não implementado!!!")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        nameLabel.text = " "
        heroImageView.image = nil
        heroImageView.kf.cancelDownloadTask()
        heroImageView.layer.removeAllAnimations()
    }
    
    func configure(with hero: MarvelHero?) {
        if hero == nil {
            print("hero nil sent to cell")
        }
        nameLabel.text = hero?.name
        heroImageView.loadImage(hero?.thumbnail?.imagePath(for: .uncanny))
    }
    
}
