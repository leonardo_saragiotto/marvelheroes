//
//  MarvelRequest.swift
//  marvelHeroes
//
//  Created by Leonardo Saragiotto on 23/04/19.
//  Copyright © 2019 Leonardo Saragiotto. All rights reserved.
//

import Alamofire
import CommonCrypto

public typealias MarvelCompletionHeroes = (_ error: MarvelError?, _ heroes: MarvelHeroesList?) -> Void
public typealias MarvelCompletionHeroeContent = (_ error: MarvelError?, _ heroes: MarvelHeroContentList?) -> Void

public protocol MarvelRequestProtocol {
    func getHeroes(_ offset: Double, completion: @escaping MarvelCompletionHeroes)
    func getHeroDetail(_ heroId: Double, completion: @escaping MarvelCompletionHeroeContent)
    func getContent(_ heroId: Double, type: HeroContentType, completion: @escaping MarvelCompletionHeroeContent)
}

public class MarvelRequest: MarvelRequestProtocol {
    
    public init() {}
    
    public func getHeroes(_ offset: Double = 0.0, completion: @escaping MarvelCompletionHeroes) {
        let url = MarvelEndpoints.Heroes.get.uri
        let method = MarvelEndpoints.Heroes.get.method
        var param = MarvelDefaults.param
        param["orderBy"] = "-modified"
        param["offset"] = offset
        
        print(param)
        
        Alamofire.request(url, method: method, parameters: param, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            switch response.result {
            case .success:
                if let data = response.data,
                    let model = try? JSONDecoder().decode(MarvelHeroesResponse.self, from: data) {
                    completion(nil, model.data)
                } else {
                    completion(MarvelError.parsingError, nil)
                }
            case .failure:
                completion(MarvelError.connectionError, nil)
            }
        }
    }
    
    public func getHeroDetail(_ heroId: Double, completion: @escaping MarvelCompletionHeroeContent) {
        let endpoint = MarvelEndpoints.Heroes.detail(heroId)
        let param = MarvelDefaults.param
        
        print(param)
        
        Alamofire.request(endpoint.uri, method: endpoint.method, parameters: param, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            switch response.result {
            case .success:
                if let data = response.data,
                    let model = try? JSONDecoder().decode(MarvelHeroContentResponse.self, from: data) {
                    let list = model.data
                    list?.contentType = .heroDetail
                    completion(nil, list)
                } else {
                    completion(MarvelError.parsingError, nil)
                }
            case .failure:
                completion(MarvelError.connectionError, nil)
            }
        }
    }
    
    public func getContent(_ heroId: Double, type: HeroContentType, completion: @escaping MarvelCompletionHeroeContent) {
        let endpoint = MarvelEndpoints.Heroes.content(heroId, type: type)
        let param = MarvelDefaults.param
        
        Alamofire.request(endpoint.uri, method: endpoint.method, parameters: param, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            switch response.result {
            case .success:
                if let data = response.data,
                    let model = try? JSONDecoder().decode(MarvelHeroContentResponse.self, from: data) {
                    let list = model.data
                    list?.contentType = type
                    completion(nil, list)
                } else {
                    completion(MarvelError.parsingError, nil)
                }
            case .failure:
                completion(MarvelError.connectionError, nil)
            }
        }
    }
}

public enum MarvelError {
    case parsingError
    case connectionError
    
    var errorDescription: String {
        switch self {
        case .connectionError:
            return NSLocalizedString("Não foi possível se conectar.", comment: "")
        case .parsingError:
            return NSLocalizedString("Não foi possível carregar as informações.", comment: "")
        }
    }
}

struct MarvelDefaults {
    static var param: [String: Any] {
        let appKey = keys?["publicKey"] as? String ?? ""
        let timestamp = Date().timeIntervalSince1970.description
        let hash = hashWith(timestamp, appKey: appKey)
        return ["apikey": appKey, "ts": timestamp, "hash": hash]
    }
    
    static private func hashWith(_ timestamp: String, appKey: String) -> String {
        let privateKey = keys?["privateKey"] as? String ?? ""
        let finalString = timestamp + privateKey + appKey
        let messageData = finalString.data(using:.utf8)!
        var digestData = Data(count: Int(CC_MD5_DIGEST_LENGTH))
        
        _ = digestData.withUnsafeMutableBytes {digestBytes in
            messageData.withUnsafeBytes {messageBytes in
                CC_MD5(messageBytes, CC_LONG(messageData.count), digestBytes)
            }
        }
        return digestData.map { String(format: "%02hhx", $0) }.joined()
        
    }
    
    static private var keys: NSDictionary? {
        if let keysPath = Bundle.main.path(forResource: "MarvelKeys", ofType: "plist"),
            let dict = NSDictionary(contentsOfFile: keysPath) {
            return dict
        }
        
        return nil
    }
}
