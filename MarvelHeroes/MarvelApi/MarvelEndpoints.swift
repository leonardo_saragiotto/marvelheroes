//
//  MarvelEndpoints.swift
//  marvelHeroes
//
//  Created by Leonardo Saragiotto on 23/04/19.
//  Copyright © 2019 Leonardo Saragiotto. All rights reserved.
//

import Alamofire

struct MarvelEndpoints {
    typealias EndpointType = (uri: String, method: Alamofire.HTTPMethod)
    
    struct Heroes {
        static let get: EndpointType = ("https://gateway.marvel.com:443/v1/public/characters", .get)
        
        static func detail(_ heroId: Double) -> EndpointType {
            return EndpointType("https://gateway.marvel.com:443/v1/public/characters/\(heroId)", .get)
        }
        
        static func content(_ heroId: Double, type: HeroContentType) -> EndpointType {
            return EndpointType("https://gateway.marvel.com:443/v1/public/characters/\(heroId)" + "/\(type.rawValue)", .get)
        }
    }
}
