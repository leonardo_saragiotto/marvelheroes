//
//  MarvelHero.swift
//  MarvelHeroes
//
//  Created by Leonardo Saragiotto on 24/04/19.
//  Copyright © 2019 Leonardo Saragiotto. All rights reserved.
//

import Foundation

public class MarvelHero: Decodable {
    public var id: Double?
    public var name: String?
    public var thumbnail: MarvelThumb?
    
    enum CodingKeys: String, CodingKey {
        case id, name, thumbnail
    }
    
    public required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try? values.decode(Double.self, forKey: .id)
        name = try? values.decode(String.self, forKey: .name)
        thumbnail = try? values.decode(MarvelThumb.self, forKey: .thumbnail)
    }
}
