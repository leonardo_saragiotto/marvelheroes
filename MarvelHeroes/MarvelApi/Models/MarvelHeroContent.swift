//
//  MarvelHeroContent.swift
//  MarvelHeroes
//
//  Created by Leonardo Saragiotto on 24/04/19.
//  Copyright © 2019 Leonardo Saragiotto. All rights reserved.
//

import Foundation

public class MarvelHeroContent: Decodable {
    public var id: Double?
    public var title: String?
    public var description: String?
    public var thumbnail: MarvelThumb?
    
    enum CodingKeys: String, CodingKey {
        case id, title, description, thumbnail, name
    }
    
    public required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try? values.decode(Double.self, forKey: .id)
        title = try? values.decode(String.self, forKey: .title)
        if title == nil {
            title = try? values.decode(String.self, forKey: .name)
        }
        description = try? values.decode(String.self, forKey: .description)
        thumbnail = try? values.decode(MarvelThumb.self, forKey: .thumbnail)
    }
}
