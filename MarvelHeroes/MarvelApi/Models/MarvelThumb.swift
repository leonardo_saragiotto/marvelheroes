//
//  MarvelThumb.swift
//  MarvelHeroes
//
//  Created by Leonardo Saragiotto on 24/04/19.
//  Copyright © 2019 Leonardo Saragiotto. All rights reserved.
//

import Foundation

public class MarvelThumb: Decodable {
    private var path: String?
    private var ext: String?
    
    enum CodingKeys: String, CodingKey {
        case path, ext = "extension"
    }
    
    public enum PortraitVariant: String {
        case small, medium, xlarge, fantastic, uncanny, incredible, full
    }
    
    public required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        path = try? values.decode(String.self, forKey: .path)
        ext = try? values.decode(String.self, forKey: .ext)
    }
    
    public func imagePath(for size: PortraitVariant) -> String? {
        
        if let path = path, let ext = ext {
            if size == .full {
                return path + "." + ext
            }
            
            return path + "/portrait_" + size.rawValue + "." + ext
        }
        
        return nil
    }
}
