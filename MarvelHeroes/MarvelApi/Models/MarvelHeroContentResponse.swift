//
//  MarvelHeroContentResponse.swift
//  MarvelHeroes
//
//  Created by Leonardo Saragiotto on 24/04/19.
//  Copyright © 2019 Leonardo Saragiotto. All rights reserved.
//

import Foundation

public class MarvelHeroContentResponse: Decodable {
    public var code: Double?
    public var status: String?
    public var data: MarvelHeroContentList?
    
    enum CodingKeys: String, CodingKey {
        case code, data, status
    }
    
    public required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        code = try? values.decode(Double.self, forKey: .code)
        status = try? values.decode(String.self, forKey: .status)
        data = try? values.decode(MarvelHeroContentList.self, forKey: .data)
    }
}

public class MarvelHeroContentList: Decodable {
    public var offset: Double?
    public var limit: Double?
    public var total: Double?
    public var count: Double?
    public var content: [MarvelHeroContent]?
    public var contentType: HeroContentType?
    
    enum CodingKeys: String, CodingKey {
        case offset, limit, total, count, results
    }
    
    public required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        offset = try? values.decode(Double.self, forKey: .offset)
        limit = try? values.decode(Double.self, forKey: .limit)
        total = try? values.decode(Double.self, forKey: .total)
        count = try? values.decode(Double.self, forKey: .count)
        content = try? values.decode([MarvelHeroContent].self, forKey: .results)
        contentType = nil
    }
}

public enum HeroContentType: String {
    case comics, stories, series, events, heroDetail
}
