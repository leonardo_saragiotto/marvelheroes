//
//  MarvelHeroesResponse.swift
//  MarvelHeroes
//
//  Created by Leonardo Saragiotto on 24/04/19.
//  Copyright © 2019 Leonardo Saragiotto. All rights reserved.
//

import Foundation

public class MarvelHeroesResponse: Decodable {
    public var code: Double?
    public var status: String?
    public var data: MarvelHeroesList?
    
    enum CodingKeys: String, CodingKey {
        case code, data, status
    }
    
    public required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        code = try? values.decode(Double.self, forKey: .code)
        status = try? values.decode(String.self, forKey: .status)
        data = try? values.decode(MarvelHeroesList.self, forKey: .data)
    }
}

public class MarvelHeroesList: Decodable {
    public var offset: Double?
    public var limit: Double?
    public var total: Double?
    public var count: Double?
    public var heroes: [MarvelHero]?
    
    enum CodingKeys: String, CodingKey {
        case offset, limit, total, count, results
    }
    
    public required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        offset = try? values.decode(Double.self, forKey: .offset)
        limit = try? values.decode(Double.self, forKey: .limit)
        total = try? values.decode(Double.self, forKey: .total)
        count = try? values.decode(Double.self, forKey: .count)
        heroes = try? values.decode([MarvelHero].self, forKey: .results)
    }
}
