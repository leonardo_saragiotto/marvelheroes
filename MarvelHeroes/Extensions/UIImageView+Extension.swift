//
//  UIImageView+Extension.swift
//  MarvelHeroes
//
//  Created by Leonardo Saragiotto on 25/04/19.
//  Copyright © 2019 Leonardo Saragiotto. All rights reserved.
//

import UIKit
import Kingfisher

extension UIImageView {
    func loadImage(_ source: String?) {
        if let url = URL(string: source ?? "") {
            self.kf.setImage(with: url,
                             placeholder: nil,
                             options: [.transition(.fade(0.4)), .cacheOriginalImage],
                             progressBlock: nil,
                             completionHandler: nil)
        }
    }
}
